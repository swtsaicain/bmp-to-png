# bmp to png
>將 bmp 圖檔轉換成 png 圖檔。


## 自行編繹產出 bmpToPng.exe
* 執行以下命令
  ```
  git clone https://gitlab.com/swtsaicain/bmp-to-png.git
  cd bmp-to-png
  load_venv.bat
  pyinstaller -F bmpToPng.py
  ```
* 產出檔案
  ```
  bmp-to-png/dist/bmpToPng.exe
  ```

## 使用方法
* 命令
  ```
  bmpToPng.exe <bmp-file> <png-file>
  ```
* 範例，將 D:\test.bmp 圖檔轉換成 png，儲存到 D:\test.png
  ```
  bmpToPng.exe D:\test.bmp D:\test.png
  ```