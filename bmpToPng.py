from PIL import Image
import argparse
# from loguru import logger


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('src_bmp', type=str, help='來源 bmp 檔')
    parser.add_argument('dst_png', type=str, help='產出 png 檔')
    args = parser.parse_args()
    src_bmp = args.src_bmp
    dst_png = args.dst_png
    # logger.debug(src_bmp)
    # logger.debug(dst_png)
    # Image.open("D:/test.bmp").save(f'D:/test.png')
    Image.open(src_bmp).save(dst_png)
